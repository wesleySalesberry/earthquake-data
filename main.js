const submitBTN = document.querySelector('.submitBTN');

submitBTN.addEventListener('click', () => {
	const searchCity = document.querySelector('.searchItem').value;
	let searchArea = document.querySelector('.searchArea').value;
	if (searchArea === '') {
		searchArea = 200;
	}
	clearField();

	geocode(searchCity, ({ latitude, longitude }) => {
		earthquakeData(searchArea, latitude, longitude);
	});
	clearCards();
});

const navLinks = document.querySelectorAll('.nav-links li a');

navLinks.forEach((element) => {
	element.addEventListener('click', () => {
		if (element.className === 'hour') {
			// pastHour('all_hour');
		}
		if (element.className === 'day') {
			pastHour('significant_day');
		}
		if (element.className === 'week') {
			pastHour('significant_week');
		}
		if (element.className === 'month') {
			pastHour('significant_month');
		}
	});
});
