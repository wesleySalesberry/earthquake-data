function geocode(zipcode, callback) {
	const GEO_URL = `https://api.mapbox.com/geocoding/v5/mapbox.places/${zipcode}.json?access_token=pk.eyJ1IjoiYXByb25hbWUiLCJhIjoiY2s5NjZxeDB0MHN5eDNncWhrNWFrbThjZiJ9.LfXRNPO3ZElv1iy43vLi1g`;

	fetch(GEO_URL)
		.then((response) => {
			return response.json();
		})
		.then((data) => {
			callback({
				latitude: data.features[0].center[1],
				longitude: data.features[0].center[0]
			});
		});
}
