const API_KEY = 'AIzaSyDWpxgomzYmN6TS4M3mQUEAyFlfv7KQ02U';

function earthquakeData(radius, latitude, longitude) {
	// if (latitude === 0 && longitude === 0 && radius == 0) {
	// 	console.log('No Lat');
	// 	URL = `https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/${timeFrame}.geojson`;
	// } else {
	// 	URL = `https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&latitude=${latitude}&longitude=${longitude}&maxradiuskm=${radius}&minmagnitude=0&limit=20&orderby=magnitude`;
	// }

	const URL = `https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&latitude=${latitude}&longitude=${longitude}&maxradiuskm=${radius}&minmagnitude=0&limit=20&orderby=magnitude`;

	fetch(URL)
		.then((response) => {
			return response.json();
		})
		.then((data) => {
			if (data.features.length === 0) {
				showAlert('No Data Found For This Area', 3000);
				return;
			}
			const resultsArr = data.features;
			resultsArr.forEach((element) => {
				const container = document.querySelector('.cardContainer');
				const data = element.properties;
				const latitude = element.geometry.coordinates[1];
				const longitude = element.geometry.coordinates[0];
				const depth = element.geometry.coordinates[2];
				const timeID = data.time;
				const updated = data.updated;
				const time = convertTime(timeID);
				const updatedTime = convertTime(updated);

				const card = `
				        <div class="card">
				            <div class="title">
				                <h2>${data.place}</h2>
				            </div>
				            <div class="mapDiv">
				                <!--<iframe width="350" height="250" frameborder="0" style="border:0"
				                src="https://www.google.com/maps/embed/v1/view?zoom=10&center=${latitude},${longitude}&key=${API_KEY}" allowfullscreen>
								</iframe>-->
								<img src="https://earthquake.usgs.gov/earthquakes/eventpage/${element.id}/map"/>
				            <div class="infoDiv">
				                <ul>
				                    <li><b><em>Type of Event:</em></b> ${data.type}</li>
				                    <li><b><em>Alert Type:</em></b> ${data.alert}</li>
				                    <li><b><em>Magnitude:</em></b> ${data.mag}</li>
				                    <li><b><em>Depth:</em></b>${depth}km</li>
				                    <li><b><em>Total People felt it:</em></b> ${data.felt}</li>
				                    <li><b><em>When it happened:</em></b> ${time}</li>
				                    <li><b><em>Last Updated:</em></b> ${updatedTime}</li>
				                </ul>
				            </div>
				                <div class="infoBTNs">
				                    <a href="https://earthquake.usgs.gov/earthquakes/eventpage/${element.id}/region-info" target="_blank" class="regionInfo">Region Info</a>
				                    <a href="https://earthquake.usgs.gov/earthquakes/eventpage/${element.id}/map" target="_blank" class="map">Interactive Map</a>
				                </div>
				        </div>
				`;

				container.innerHTML += card;
			});
		});
}

function pastHour(timeFrame) {
	const URL = `https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/${timeFrame}.geojson`;
	fetch(URL)
		.then((response) => {
			return response.json();
		})
		.then((data) => {
			if (data.features.length === 0) {
				showAlert('No Data Found for this time frame', 3000);
				return;
			}
			const resultsArr = data.features;
			resultsArr.forEach((element) => {
				const container = document.querySelector('.cardContainer');
				const data = element.properties;
				const latitude = element.geometry.coordinates[1];
				const longitude = element.geometry.coordinates[0];
				const depth = element.geometry.coordinates[2];
				const timeID = data.time;
				const updated = data.updated;
				const time = convertTime(timeID);
				const updatedTime = convertTime(updated);

				console.log(element.id);

				const card = `
				        <div class="card">
				            <div class="title">
				                <h2>${data.place}</h2>
				            </div>
				            <div class="mapDiv">
				               <!-- <iframe width="350" height="250" frameborder="0" style="border:0"
				                src="https://www.google.com/maps/embed/v1/view?zoom=10&center=${latitude},${longitude}&key=${API_KEY}" allowfullscreen>
				                </iframe>-->
				            </div>
				            <div class="infoDiv">
				                <ul>
				                    <li><b><em>Type of Event:</em></b> ${data.type}</li>
				                    <li><b><em>Alert Type:</em></b> ${data.alert}</li>
				                    <li><b><em>Magnitude:</em></b> ${data.mag}</li>
				                    <li><b><em>Depth:</em></b> ${depth}km</li>
				                    <li><b><em>Total People felt it:</em></b> ${data.felt}</li>
				                    <li><b><em>When it happened:</em></b> ${time}</li>
				                    <li><b><em>Last Updated:</em></b> ${updatedTime}</li>
				                </ul>
				            </div>
				                <div class="infoBTNs">
				                    <a href="https://earthquake.usgs.gov/earthquakes/eventpage/${data.id}/region-info" target="_blank" class="regionInfo">Region Info</a>
				                    <a href="#" class="map">Interactive Map</a>
				                </div>
				        </div>
				`;

				container.innerHTML += card;
			});
		});
}

//Helpwe function to convert long int time into a readable format
function convertTime(time) {
	let date = new Date(time);
	let options = {
		weekday: 'short',
		year: 'numeric',
		month: '2-digit',
		day: 'numeric',
		hour: 'numeric',
		minute: 'numeric'
	};

	const startDate = date.toLocaleDateString('en', options);

	return startDate;
}
