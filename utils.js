// TODO add smooth scroll
// TODO Hook up links for the search and links on nav bar

navSlide();

function navSlide() {
	const sideNav = document.querySelector('.side-nav');
	const nav = document.querySelector('.nav-links');
	const navLinks = document.querySelectorAll('.nav-links li');

	//toggle
	sideNav.addEventListener('click', () => {
		nav.classList.toggle('nav-active');

		navLinks.forEach((link, index) => {
			if (link.style.animation) {
				link.style.animation = '';
			} else {
				link.style.animation = `navLinkFade 0.5s ease forwards ${index / 5 + 0.5}s`;
			}
		});
	});
}

function showAlert(message, time) {
	const cardContainer = document.querySelector('.cardContainer');
	const alertDiv = document.createElement('div');
	alertDiv.className = 'alert';

	alertDiv.appendChild(document.createTextNode(message));
	cardContainer.appendChild(alertDiv);

	setTimeout(() => {
		document.querySelector(`.alert`).remove();
	}, time);
}

function clearField() {
	// document.querySelector('input').value = '';
	const searchCity = document.querySelector('.searchItem');
	const searchArea = document.querySelector('.searchArea');

	searchCity.value = '';
	searchArea.value = '';
}

function clearCards() {}
