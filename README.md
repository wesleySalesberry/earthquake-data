<h1 align="center">Welcome to earthquake-data 👋</h1>
<p>
</p>

> Allows you to look up earthquakes in regions and gives data back in a user defined area in kilometers. Also displays the location in a google embedded map. 

### 🏠 [Homepage](https://wesleysalesberry.gitlab.io/earthquake-data/)

### ✨ [Demo](https://wesleysalesberry.gitlab.io/earthquake-data/)

## Author

👤 **Wesley Salesberry**


## Show your support

Give a ⭐️ if this project helped you!

***
